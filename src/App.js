import { useState } from "react";

let calculator = 3;

function Placeholder() {
  return <div className="bg-gray-200 animate-pulse w-full h-full flex items-center justify-center">
    generation
  </div>
}

function App() {
  const [variationsTree, setVariationsTree] = useState([
    {
      id: 1,
      parentId: null,
      filters: ["red", "buckle", "long sleeve"],
      level: 0,
      status: "generated",
    },
    {
      id: 2,
      parentId: null,
      filters: ["red", "buckle", "long sleeve"],
      level: 0,
      status: "generated",
    },
    {
      id: 3,
      parentId: null,
      filters: ["red", "buckle", "long sleeve"],
      level: 0,
      status: "generated",
    },
  ]);

  const [selectedVariationId, setSelectedVariationId] = useState(null);
  const [additionalKeywords, setAdditionalKeywords] = useState([]);
  const [keywordInput, setKeywordInput] = useState("");

  const selectedVariation = variationsTree.find(el => el.id === selectedVariationId)

  const generate = () => {
    const newVariation = {
      id: ++calculator,
      parentId: selectedVariation.id,
      filters: [...selectedVariation.filters, ...additionalKeywords],
      level: selectedVariation.level + 1,
      ts: Date.now(),
      status: "new",
    };

    setTimeout(() => {
      setVariationsTree((s) =>
        s.map((el) =>
          el.id === newVariation.id ? { ...el, status: "generate" } : el
        )
      );
    }, 10000);

    setVariationsTree([...variationsTree, newVariation]);
    setAdditionalKeywords([]);
  };

  const handleChange = (e) => {
    if (e.key === "Enter") {
      if (
        [...additionalKeywords, selectedVariation.filters]
          .flat()
          .includes(keywordInput)
      ) {
        return;
      }
      setAdditionalKeywords([...additionalKeywords, keywordInput]);
      setKeywordInput("");
    }
  };

  const levels = variationsTree.reduce((acc, el) => {
    if (el.level > acc) {
      return el.level;
    }
    return acc;
  }, 0);

  const findAllChildsRecursevily = (id) => {
    const childs = variationsTree.filter((el) => el.parentId === id);
    if (childs.length === 0) {
      return [];
    }
    return childs.concat(
      ...childs.map((el) => findAllChildsRecursevily(el.id))
    );
  };

  const findAllDirectChilds = (id) => {
    const childs = variationsTree.filter((el) => el.parentId === id);
    if (childs.length === 0) {
      return [];
    }
    return childs;
  };

  const findAllParentsRecursevily = (id) => {
    const parent = variationsTree.find((el) => el.id === id);
    if (!parent) {
      return [];
    }
    return [parent].concat(findAllParentsRecursevily(parent.parentId));
  };

  const findAllSiblings = (id) => {
    const parent = variationsTree.find((el) => el.id === id);
    if (!parent) {
      return [];
    }
    return variationsTree
      .filter((el) => el.parentId === parent.parentId)
      .filter((el) => el.id !== id);
  };

  const findAllParentsAndTheirSiblings = (id) => {
    const parents = findAllParentsRecursevily(id);
    return parents.concat(...parents.map((el) => findAllSiblings(el.id)));
  };

  const uniqBy = (arr, key) => {
    return [...new Map(arr.map((item) => [item[key], item])).values()];
  };

  const childs = selectedVariation && findAllDirectChilds(selectedVariation.id);
  const parents =
    selectedVariation && findAllParentsAndTheirSiblings(selectedVariation.id);
  const siblings = selectedVariation && findAllSiblings(selectedVariation.id);

  const family =
    selectedVariation && uniqBy([...childs, ...parents, ...siblings], "id");

  return (
    <div className="flex">
      <div className="shadow h-screen p-8 overflow-y-auto">
        <div className="font-bold mt-2">Baseline</div>

        <div className="grid grid-cols-3 gap-4 mb-4">
          {variationsTree
            .filter((el) => el.level === 0)
            .map((variation) => {
              const childsCount = findAllChildsRecursevily(variation.id).length;
              return (
                <div
                  onClick={() => setSelectedVariationId(variation.id)}
                  className={
                    "relative w-[100px] h-[100px] " +
                    (variation.id === selectedVariation?.id
                      ? "border-2 border-red-600"
                      : "")
                  }
                >
                  {variation.status === "new" ? (
                   <Placeholder />
                  ) : (
                    <img
                      src={`https://loremflickr.com/600/600?lock=${variation.id}`}
                      alt=""
                      className=" "
                    />
                  )}

                  <div className="absolute bottom-0 right-2 text-white font-bold">
                    {childsCount}
                  </div>
                </div>
              );
            })}
        </div>

        {selectedVariation &&
          Array.from({ length: levels }).map((_, i) => {
            const items = family
              .filter((el) => el.level === i + 1)
              .sort((a, b) => a.ts - b.ts);
            if (items.length === 0) return null;

            return (
              <>
                <div className="font-bold mt-2">Iteration: {i + 1}</div>
                <div className="grid grid-cols-3 gap-4">
                  {items.map((variation) => {
                    const childsCount = findAllChildsRecursevily(
                      variation.id
                    ).length;
                    return (
                      <div
                        key={variation.id}
                        onClick={() => setSelectedVariationId(variation.id)}
                        className={
                          "relative w-[100px] h-[100px] " +
                          (variation.id === selectedVariation?.id
                            ? "border-2 border-red-600"
                            : "")
                        }
                      >
                        {variation.status === "new" ? (
                         <Placeholder />
                        ) : (
                          <img
                            src={`https://loremflickr.com/600/600?lock=${variation.id}`}
                            alt=""
                            className=""
                          />
                        )}

                        <div className="absolute bottom-0 right-2 text-white font-bold">
                          {childsCount}
                        </div>
                      </div>
                    );
                  })}
                </div>
              </>
            );
          })}
      </div>

      <div className="flex-grow flex items-center justify-center p-8">
        {selectedVariation ? (
          selectedVariation.status === "new" ? (
           <Placeholder />
          ) : (
            <img
              src={`https://loremflickr.com/600/600?lock=${selectedVariation.id}`}
              alt=""
            />
          )
        ) : (
          "select image on the left first"
        )}
      </div>

      <div className="shadow h-screen w-[400px] p-8">
        {selectedVariation && (
          <div className="flex flex-col justify-between h-full">
            <div className="">
              <div>
                <h3 className="text-lg font-bold mt-4">Used kewords</h3>
                {selectedVariation.filters.map((filter) => (
                  <div>{filter}</div>
                ))}
              </div>
              <h3 className="text-lg font-bold mt-4">Additional kewords</h3>
              <input
                type="text"
                className="border border-gray-200 rounded py-1 px-2 w-full"
                value={keywordInput}
                onChange={(e) => setKeywordInput(e.target.value)}
                onKeyPress={handleChange}
              />
              {additionalKeywords.map((keyword) => (
                <div>
                  {keyword}
                  <button
                    className="p-1 text-red-500 "
                    onClick={() => {
                      setAdditionalKeywords(
                        additionalKeywords.filter((k) => k !== keyword)
                      );
                    }}
                  >
                    x
                  </button>
                </div>
              ))}
            </div>

            <button
              className="py-1 px-2 border border-blue-300 rounded hover:bg-blue-300 hover:text-white"
              onClick={generate}
            >
              Generate
            </button>
          </div>
        )}
      </div>
    </div>
  );
}

export default App;
